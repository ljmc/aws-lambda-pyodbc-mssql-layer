ARG PYTHON_VER

FROM public.ecr.aws/lambda/python:${PYTHON_VER} as builder

ARG PYODBC_VER
ARG UNIXODBC_VER
ARG MSODBCSQL_VER

ADD https://www.unixodbc.org/unixODBC-${UNIXODBC_VER}.tar.gz .
ADD https://packages.microsoft.com/config/rhel/8/prod.repo /etc/yum.repos.d/mssql-release.repo

RUN yum install -y gcc gcc-c++ gzip make openssl tar zip && \
	ACCEPT_EULA=Y yum install -y --disablerepo=amzn* msodbcsql${MSODBCSQL_VER} && \
	tar xzf unixODBC-${UNIXODBC_VER}.tar.gz && \
	cd unixODBC-${UNIXODBC_VER} && \
	./configure \
		--prefix=/opt/unixODBC-${UNIXODBC_VER} \
		--sysconfdir=/opt \
		--disable-gui \
		--disable-drivers \
		--enable-iconv \
		--with-iconv-char-enc=UTF8 \
		--with-iconv-ucode-enc=UTF16LE && \
	make install && \
	cd .. && \
	rm -r unixODBC-${UNIXODBC_VER}.tar.gz unixODBC-${UNIXODBC_VER} && \
	mkdir /opt/lib && \
	cd /opt/lib && \
	ln --symlink ../unixODBC-${UNIXODBC_VER}/lib/*.so* . && \
	cd /opt && \
	ln -sr ../microsoft/msodbcsql${MSODBCSQL_VER}/etc/odbcinst.ini . && \
	CFLAGS="-I/opt/unixODBC-${UNIXODBC_VER}/include" \
		LDFLAGS="-L/opt/unixODBC-${UNIXODBC_VER}/lib" \
		pip install --no-cache-dir --target /opt/python pyodbc==${PYODBC_VER} && \
	cd /opt && \
	zip --recursive --symlinks /layer.zip .
